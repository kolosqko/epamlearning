import Foundation

class BinaryTree {
    var root: Node?
    
    private func addRecursive(currentNode: Node?, value: Int) -> Node {
        guard let currentNode = currentNode else {
            return Node(withValue: value)
        }
        
        if value < currentNode.value {
            currentNode.left = addRecursive(currentNode: currentNode.left, value: value)
        }
        if value > currentNode.value {
            currentNode.right = addRecursive(currentNode: currentNode.right, value: value)
        }
        return currentNode
    }
    
    private func containsValueRecursive(currentNode: Node?, value: Int) -> Bool {
        guard let currentNode = currentNode else {
            return false
        }
        if currentNode.value == value {
            return true
        }
        
        return value < currentNode.value
            ? containsValueRecursive(currentNode: currentNode.left, value: value)
            : containsValueRecursive(currentNode: currentNode.right, value: value)
    }
    
    private func deleteNodeRecursive(currentNode: Node?, value: Int) -> Node? {
        guard let currentNode = currentNode else {
            return nil
        }
        if currentNode.value == value {
            // if node is a leaf
            if currentNode.left == nil && currentNode.right == nil {
                return nil
            }
            // if node has only 1 branch
            
            if currentNode.left == nil {
                return currentNode.right
            }
            guard let currentNodesRight = currentNode.right else {
                return currentNode.left
            }
            // if node has 2 branches
            let smallestValue = findSmallestValue(root: currentNodesRight)
            currentNode.right = deleteNodeRecursive(currentNode: currentNode.right, value: smallestValue)
            currentNode.value = smallestValue
            return currentNode
        }
        if value < currentNode.value {
            currentNode.left = deleteNodeRecursive(currentNode: currentNode.left, value: value)
        } else {
            currentNode.right = deleteNodeRecursive(currentNode: currentNode.right, value: value)
        }
        return currentNode
    }
    
    private func findSmallestValue(root: Node) -> Int {
        guard let leftBranchRoot = root.left else {
            return root.value
        }
        return findSmallestValue(root: leftBranchRoot)
    }
    
    private func traverseInOrderRecursive(currentNode: Node?, _ block: (Int) -> ()) {
        guard let currentNode = currentNode else {
            return
        }
        
        traverseInOrderRecursive(currentNode: currentNode.left, block)
        block(currentNode.value)
        traverseInOrderRecursive(currentNode: currentNode.right, block)
    }
    
    private func traversePreOrderRecursive(currentNode: Node?, _ block: (Int) -> ()) {
        guard let currentNode = currentNode else {
            return
        }
        
        block(currentNode.value)
        traversePreOrderRecursive(currentNode: currentNode.left, block)
        traversePreOrderRecursive(currentNode: currentNode.right, block)
    }
    
    private func traversePostOrderRecursive(currentNode: Node?, _ block: (Int) -> ()) {
        guard let currentNode = currentNode else {
            return
        }

        traversePostOrderRecursive(currentNode: currentNode.left, block)
        traversePostOrderRecursive(currentNode: currentNode.right, block)
        block(currentNode.value)
    }
    
    
    func addNode(withValue value: Int) {
        root = addRecursive(currentNode: root, value: value)
    }
    
    func containsValue(value: Int) -> Bool {
        return containsValueRecursive(currentNode: root, value: value)
    }
    
    func deleteNode(withValue value: Int) {
        root = deleteNodeRecursive(currentNode: root, value: value)
    }
    
    func traverseInOrder( _ block: (Int) -> ()) {
        traverseInOrderRecursive(currentNode: root, block)
        print()
    }
    
    func traversePreOrder( _ block: (Int) -> ()) {
        traversePreOrderRecursive(currentNode: root, block)
        print()
    }
    
    func traversePostOrder( _ block: (Int) -> ()) {
        traversePostOrderRecursive(currentNode: root, block)
        print()
    }
    
    func traverseBreadthFirst( _ block: (Int) -> ()) {
        guard let root = root else {
            return
        }
        var nodes: [Node] = []
        nodes.append(root)
        while (nodes.count != 0) {
            let node = nodes[0]
            nodes.remove(at: 0)
            block(node.value)
            if let nodeLeft = node.left {
                nodes.append(nodeLeft)
            }
            if let nodeRight = node.right {
                nodes.append(nodeRight)
            }
        }
    }

    
    
    
}

class Node{
    var value: Int
    var left: Node?
    var right: Node?
    
    init(withValue value: Int) {
        self.value = value
    }
    
}


//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------




let binaryTree = BinaryTree()

binaryTree.addNode(withValue: 14)
binaryTree.addNode(withValue: 3)
binaryTree.addNode(withValue: 7)
binaryTree.addNode(withValue: 18)
binaryTree.addNode(withValue: 23)
binaryTree.addNode(withValue: 19)
binaryTree.addNode(withValue: -3)
binaryTree.addNode(withValue: 4)
binaryTree.addNode(withValue: -65)
binaryTree.addNode(withValue: 32)


binaryTree.containsValue(value: 23)
binaryTree.containsValue(value: 19)
binaryTree.containsValue(value: -3)
binaryTree.containsValue(value: 123)
binaryTree.containsValue(value: 13)


let printBlock: (Int) -> () = {value in
    print(value, terminator: " ")
}

binaryTree.traverseInOrder(printBlock)
print()
binaryTree.traversePreOrder(printBlock)
print()
binaryTree.traversePostOrder(printBlock)
print()
binaryTree.traverseBreadthFirst(printBlock)
print()



binaryTree.deleteNode(withValue: 3)
binaryTree.containsValue(value: 3)
binaryTree.deleteNode(withValue: 19)
binaryTree.containsValue(value: 19)
binaryTree.addNode(withValue: 9)
binaryTree.containsValue(value: 9)



print(binaryTree.root == nil)
print(binaryTree.root?.left == nil)
print(binaryTree.root?.right == nil)
print(binaryTree.root?.left?.right == nil)



binaryTree.traverseInOrder(printBlock)
print()
binaryTree.traversePreOrder(printBlock)
print()
binaryTree.traversePostOrder(printBlock)
print()
binaryTree.traverseBreadthFirst(printBlock)
print()


