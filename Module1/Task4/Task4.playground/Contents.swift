import Foundation

//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------

// Task4.1

enum QuadraticEquationError: LocalizedError {
    case negativeDiscriminant(discriminant: Double)
    // a = b = 0
    case quadraticAndLinearCoefficientsEqualZero(c: Double)
    
    public var errorDescription: String? {
        switch self {
        case .negativeDiscriminant(let d):
            return "Discriminant equels \(d), which is less then 0"
        case .quadraticAndLinearCoefficientsEqualZero(let c):
            if (c == 0) {
                return "There is infinite number of solutions, because coefficients equal 0"
            } else {
                return "Equation is imposible, becouse \(c) doesn't equal 0"
            }
        }
    }
}


func rootsOfQuadraticEquation(_ a: Double, _ b: Double, _ c: Double) throws -> (Double, Double) {

    if a == 0 {
        if b == 0 {
            throw QuadraticEquationError.quadraticAndLinearCoefficientsEqualZero(c: c)
        }
        let root = -c / b
        return (root, root)
    }
    
    let d = pow(b, 2) - 4*c*a
    if d < 0 {
        throw QuadraticEquationError.negativeDiscriminant(discriminant: d)
    }
    if d == 0 {
        let root = -b / (2 * a)
        return (root, root)
    }
    let root1 =  ( -b + pow(d, 0.5)) / (2 * a)
    let root2 =  ( -b - pow(d, 0.5)) / (2 * a)
    
    return (root1, root2)
}

func getQuadraticEquationString(_ a: Double, _ b: Double, _ c: Double) -> String {
    if a == 0 {
        if c == 0 {
            return "\(b)*x = 0, "
        }
        return "\(b)*x + \(c) = 0, "
    }
    
    if c == 0 {
        if b == 0 {
            return "\(a)*x^2 = 0, "
        }
        return "\(a)*x^2 + \(b)*x = 0, "
    }
    
    if b == 0 {
        return "\(a)*x^2 + \(c) = 0, "
    }
    
    return "\(a)*x^2 + \(b)*x + \(c) = 0, "
}

func getQuadraticEquationResult(roots: (Double, Double)) -> String {
    if roots.0 == roots.1 {
        return "root = \(roots.0)"
    }
    return "root1 = \(roots.0), root2 = \(roots.1)"
}

//--------------------------------------------------------------------------------------------------------------------------------
print("Output for quadratic equations with only real solutions:")


var a: Double = 14
var b: Double = 329
var c: Double = 233


do {
    let roots = try rootsOfQuadraticEquation(a, b, c)
    let printString = getQuadraticEquationString(a, b, c) + getQuadraticEquationResult(roots: roots)
    print(printString)
} catch let error {
    print(error.localizedDescription)
}


//--------------------------------------------------------------------------------------------------------------------------------


a = 0
b = 32
c = 17

do {
    let roots = try rootsOfQuadraticEquation(a, b, c)
    let printString = getQuadraticEquationString(a, b, c) + getQuadraticEquationResult(roots: roots)
    print(printString)
} catch let error {
    print(error.localizedDescription)
}


//--------------------------------------------------------------------------------------------------------------------------------


a = 34
b = 0
c = 10

do {
    let roots = try rootsOfQuadraticEquation(a, b, c)
    let printString = getQuadraticEquationString(a, b, c) + getQuadraticEquationResult(roots: roots)
    print(printString)
} catch let error {
    print(error.localizedDescription)
}

//--------------------------------------------------------------------------------------------------------------------------------


a = 34
b = 0
c = -10

do {
    let roots = try rootsOfQuadraticEquation(a, b, c)
    let printString = getQuadraticEquationString(a, b, c) + getQuadraticEquationResult(roots: roots)
    print(printString)
} catch let error {
    print(error.localizedDescription)
}


//--------------------------------------------------------------------------------------------------------------------------------


a = 0
b = 0
c = -10

do {
    let roots = try rootsOfQuadraticEquation(a, b, c)
    let printString = getQuadraticEquationString(a, b, c) + getQuadraticEquationResult(roots: roots)
    print(printString)
} catch let error {
    print(error.localizedDescription)
}

//--------------------------------------------------------------------------------------------------------------------------------


a = 0
b = 0
c = 0

do {
    let roots = try rootsOfQuadraticEquation(a, b, c)
    let printString = getQuadraticEquationString(a, b, c) + getQuadraticEquationResult(roots: roots)
    print(printString)
} catch let error {
    print(error.localizedDescription)
}



//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------

// Task4.2

struct Complex {
    let real: Double
    let imaginary: Double
    
    init(real: Double, imaginary: Double) {
        self.real = real
        self.imaginary = imaginary
    }
    
    init(_ real: Double) {
        self.real = real
        self.imaginary = 0
    }
    
    init() {
        self.real = 0
        self.imaginary = 0
    }
    
    
    static let zero = Complex()
    
    var toString: String {
        return "\(real) + \(imaginary) * i"
    }
}


//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------

// Task4.3


extension Complex {
    
    static func +(lhs: Complex, rhs: Complex) -> Complex {
        let result = Complex(real: lhs.real + rhs.real, imaginary: lhs.imaginary + rhs.imaginary)
        return result
    }
    
    static func -(lhs: Complex, rhs: Complex) -> Complex {
        let result = Complex(real: lhs.real - rhs.real, imaginary: lhs.imaginary - rhs.imaginary)
        return result
    }
    
    static func *(lhs: Complex, rhs: Complex) -> Complex {
        let resultRealPart = (lhs.real * rhs.real - lhs.imaginary * rhs.imaginary)
        let resultImaginaryRart = (rhs.real * lhs.imaginary + lhs.real * rhs.imaginary)
        let result = Complex(real: resultRealPart, imaginary: resultImaginaryRart)
        return result
    }
    
    static func /(lhs: Complex, rhs: Complex) -> Complex {
                guard rhs.real != 0 && rhs.imaginary != 0 else {
                    fatalError("divition by 0")
                }
        let resultRealPart = ( lhs.real * rhs.real + lhs.imaginary * rhs.imaginary) / ( pow(rhs.real, 2) + pow(rhs.imaginary, 2) )
        let resultImaginaryRart = ( lhs.imaginary * rhs.real - lhs.real * rhs.imaginary) / ( pow(rhs.real, 2) + pow(rhs.imaginary, 2) )
        let result = Complex(real: resultRealPart, imaginary: resultImaginaryRart)
        return result
    }
    
    static prefix func -(rhs: Complex) -> Complex {
        let result = Complex(real: -rhs.real, imaginary: -rhs.imaginary)
        return result
    }
    
    static func ==(lhs: Complex, rhs: Complex) -> Bool {
        if lhs.real == rhs.real && lhs.imaginary == rhs.imaginary {
            return true
        }
        return false
    }
    
    static func !=(lhs: Complex, rhs: Complex) -> Bool {
        if lhs.real == rhs.real && lhs.imaginary == rhs.imaginary {
            return false
        }
        return true
    }
    
    static func +=( lhs: inout Complex, rhs: Complex)  {
        lhs = lhs + rhs
    }
    
    static func -=( lhs: inout Complex, rhs: Complex)  {
        lhs = lhs - rhs
    }
    
    static func *=( lhs: inout Complex, rhs: Complex)  {
        lhs = lhs * rhs
    }
    
    static func /=( lhs: inout Complex, rhs: Complex)  {
        lhs = lhs / rhs
    }

    
}

func pow(_ x: Complex, _ y: Int) -> Complex {
    
    if x == Complex.zero {
        return x
    }
    
    if y <= 0{
        if y == 0 {
            return Complex(1.0)
        }
        var result = Complex(1.0) / x
        result /= pow(x, y + 1)
        return result
    }
    
    if y == 1 {
        return x
    }
    var result = x
    result *= pow(x, y - 1)
    return result
}




//-------------------------------------

print("-------------------------------------------------------------")
print("Output for operations with complex numbers:")


let x = Complex(real: 3, imaginary: 2)
let y = Complex(real: 4, imaginary: -3)
let inPowerOf: Int = 4

let sum = x + y
let difference = x - y
let multiplicationResult = x * y
let divisionResult = x / y
let powResult = pow(x, inPowerOf)

let sumString = "(\(x.toString)) + (\(y.toString)) = (\(sum.toString))"
let differenceString = "(\(x.toString)) - (\(y.toString)) = (\(difference.toString))"
let multiplicationResultString = "(\(x.toString)) * (\(y.toString)) = (\(multiplicationResult.toString))"
let divisionResultString = "(\(x.toString)) / (\(y.toString)) = (\(divisionResult.toString))"
let powString = "(\(x.toString)) ^ (\(inPowerOf)) = (\(powResult.toString))"


print(sumString)
print(differenceString)
print(multiplicationResultString)
print(divisionResultString)
print(powString)




//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------

// Task4.4


func complexRootsOfQuadraticEquation(_ a: Double, _ b: Double, _ c: Double) throws -> (Complex, Complex) {
    
//    let a = Complex(a)
//    let b = Complex(b)
//    let c = Complex(c)

    
    if a == 0 {
        if b == 0 {
            throw QuadraticEquationError.quadraticAndLinearCoefficientsEqualZero(c: c)
        }
        let root = Complex(-c / b)
        return (root, root)
    }
    
    let d = pow(b, 2) - 4 * c * a
    
    if d == 0 {
        let root = Complex(-b / (2 * a))
        return (root, root)
    }
    
    if d < 0 {
        let root1 = Complex(real: -b / (2 * a), imaginary: pow(abs(d), 0.5) / (2 * a))
        let root2 = Complex(real: -b / (2 * a), imaginary: -(pow(abs(d), 0.5) / (2 * a)))
        return (root1, root2)
    }
    
    let root1 = Complex( -b / (2 * a) + pow(d, 0.5) / (2 * a) )
    let root2 = Complex( -b / (2 * a) - pow(d, 0.5) / (2 * a) )
    
    return (root1, root2)
}



func getComplexQuadraticEquationResult(roots: (Complex, Complex)) -> String {
    if roots.0 == roots.1 {
        return "root = \(roots.0)"
    }
    return "root1 = \(roots.0.toString), root2 = \(roots.1.toString)"
}

//-------------------------------------------------------------

print("-------------------------------------------------------------")
print("For complex numbers:")

a = 14
b = 329
c = 233


do {
    let roots = try complexRootsOfQuadraticEquation(a, b, c)
    let printString = getQuadraticEquationString(a, b, c) + getComplexQuadraticEquationResult(roots: roots)
    print(printString)
} catch let error {
    print(error.localizedDescription)
}

//--------------------------------------------------------------------------------------------------------------------------------


a = 0
b = 32
c = 17

do {
    let roots = try complexRootsOfQuadraticEquation(a, b, c)
    let printString = getQuadraticEquationString(a, b, c) + getComplexQuadraticEquationResult(roots: roots)
    print(printString)
} catch let error {
    print(error.localizedDescription)
}
//--------------------------------------------------------------------------------------------------------------------------------


a = 34
b = 1
c = 10

do {
    let roots = try complexRootsOfQuadraticEquation(a, b, c)
    let printString = getQuadraticEquationString(a, b, c) + getComplexQuadraticEquationResult(roots: roots)
    print(printString)
} catch let error {
    print(error.localizedDescription)
}

//--------------------------------------------------------------------------------------------------------------------------------


a = 34
b = 0
c = -10

do {
    let roots = try complexRootsOfQuadraticEquation(a, b, c)
    let printString = getQuadraticEquationString(a, b, c) + getComplexQuadraticEquationResult(roots: roots)
    print(printString)
} catch let error {
    print(error.localizedDescription)
}

//--------------------------------------------------------------------------------------------------------------------------------


a = 0
b = 0
c = -10

do {
    let roots = try complexRootsOfQuadraticEquation(a, b, c)
    let printString = getQuadraticEquationString(a, b, c) + getComplexQuadraticEquationResult(roots: roots)
    print(printString)
} catch let error {
    print(error.localizedDescription)
}
//--------------------------------------------------------------------------------------------------------------------------------


a = 0
b = 0
c = 0

do {
    let roots = try complexRootsOfQuadraticEquation(a, b, c)
    let printString = getQuadraticEquationString(a, b, c) + getComplexQuadraticEquationResult(roots: roots)
    print(printString)
} catch let error {
    print(error.localizedDescription)
}
