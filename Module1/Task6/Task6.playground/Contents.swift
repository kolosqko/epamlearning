import Foundation

func evenNumbersFromArray(_ array: [Int]) -> [Int] {
    return array.filter { $0 % 2 == 0}
}

func oddNumbersFromArray(_ array: [Int]) -> [Int] {
    return array.filter { abs($0 % 2) == 1}
}

func sumOfNumbersInArray(_ array: [Int]) -> Int {
    return array.reduce(0, +)
}

func arrayOfStringsToArrayOfInt(_ array: [String]) -> [Int] {
    return array.compactMap { Int($0) }
}

let arrayInt = [1, 2, 12, -34, 12, -5, 7, 88, 1004]
let arrayString = ["1", "2", "12", "bla", "5", "7", "88", "asfad", "-423"]

let evenNumbers = evenNumbersFromArray(arrayInt)
let oddNumbers = oddNumbersFromArray(arrayInt)
let sum = sumOfNumbersInArray(arrayInt)
let arrayOfIntFromString = arrayOfStringsToArrayOfInt(arrayString)

print("array of integers: \(arrayInt)")
print("array of Strings: \(arrayString)")

print("even numbers: \(evenNumbers)")
print("odd numbers: \(oddNumbers)")
print("sum of numbers: \(sum)")
print("converted from [String]: \(arrayOfIntFromString)")
