import Foundation

// Я абсолютно не впевнений чи правильно я зрозумів завдання.

enum MyOptional<Wrapper> {
    case some(Wrapper)
    case none
    
    private var isNil: Bool {
        switch self {
        case .none:
            return true
        default:
            return false
        }
    }
}

extension MyOptional where Wrapper == Int {
    
    static func ==(lhs: MyOptional<Int>, rhs: MyOptional<Int>) -> Bool {
        switch lhs {
        case .none:
            switch rhs {
            case .none:
                return true
            case .some:
                return false
            }
        case .some(let leftValue):
            switch rhs {
            case .none:
                return false
            case .some(let rightValue):
                return leftValue == rightValue
            }
        }
    }
    
    static func !=(lhs: MyOptional<Int>, rhs: MyOptional<Int>) -> Bool {
        return !(lhs == rhs)
    }
    
    static func +(lhs: MyOptional<Int>, rhs: MyOptional<Int>) -> MyOptional<Int>{
        switch lhs {
        case .none:
            return .none
        case .some(let leftValue):
            switch rhs {
            case .none:
                return .none
            case .some(let rightValue):
                return .some(leftValue + rightValue)
            }
        }
    }
    
    static func -(lhs: MyOptional<Int>, rhs: MyOptional<Int>) -> MyOptional<Int>{
        switch lhs {
        case .none:
            return .none
        case .some(let leftValue):
            switch rhs {
            case .none:
                return .none
            case .some(let rightValue):
                return .some(leftValue - rightValue)
            }
        }
    }
    
    static func *(lhs: MyOptional<Int>, rhs: MyOptional<Int>) -> MyOptional<Int>{
        switch lhs {
        case .none:
            return .none
        case .some(let leftValue):
            switch rhs {
            case .none:
                return .none
            case .some(let rightValue):
                return .some(leftValue * rightValue)
            }
        }
    }
    
    static func /(lhs: MyOptional<Int>, rhs: MyOptional<Int>) -> MyOptional<Int>{
        switch lhs {
        case .none:
            return .none
        case .some(let leftValue):
            switch rhs {
            case .none:
                return .none
            case .some(let rightValue):
                return .some(leftValue / rightValue)
            }
        }
    }
}

let a: MyOptional<Int> = .some(2)
let b: MyOptional<Int> = .some(4)
let c: MyOptional<Int> = .none
let d: MyOptional<Int> = .some(-2)
let e: MyOptional<Int> = .none


print(a + b)
print(a - b)
print(a * b)
print(a / b)
print(a == b)
print(a + c)
print(a + b)
print(b + e)
print(e == c)
print(d == c)
