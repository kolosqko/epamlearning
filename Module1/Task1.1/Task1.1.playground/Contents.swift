import UIKit
import Foundation


// func for getting an Int result
// will allways be rounded down

func arithmeticMean(numbers: Int...) -> Int {
    var sum: Int = 0
    numbers.forEach { number in
        sum += number
    }
    let mean = sum / numbers.count
    return mean
}



// func for getting a Double result

func arithmeticMeanDouble(numbers: Int...) -> Double {
    var sum: Double = 0
    numbers.forEach { number in
        sum += Double(number)
    }
    let mean = sum / Double(numbers.count)
    return mean
}

// for geometric mean the result wil be nil if result of multiplying numbers is less the 0 and number of munbers is even

func geometricMean(numbers: Int...) -> Double? {
    var product: Double = 1
    numbers.forEach { number in
        product *= Double(number)
    }
    if product < 0, numbers.count % 2 == 0 {
        return nil
    }
    let mean = pow(product, 1.0/Double(numbers.count))
    return mean
}

// -----------------------------


arithmeticMean(numbers: 13, 213, -1250, 71)
arithmeticMeanDouble(numbers: 5, -6, 140, 21, 11)
geometricMean(numbers: 9, 3, 1)
geometricMean(numbers: 9, 3, 1, -1)
geometricMean(numbers: 109, 32, 1, 5, 34, 1234)
