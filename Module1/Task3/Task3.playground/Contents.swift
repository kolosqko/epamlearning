
let array: [Int] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

// print all

func arraysValuesForInToString(array: [Int]) -> String {
    var str = ""
    for item in array {
        if str.count > 0 {
            str += ", "
        }
        str += String(item)
    }
    return str
}

func arraysValuesWhileToString(array: [Int]) -> String {
    var str = ""
    var index = 0
    while index < array.count {
        if str.count > 0 {
            str += ", "
        }
        str += String(array[index])
        index += 1
    }
    return str
}

func arraysValuesRepeatWhileToString(array: [Int]) -> String {
    var str = ""
    var index = 0
    repeat {
        if str.count > 0 {
            str += ", "
        }
        str += String(array[index])
        index += 1
    } while index < array.count
    return str
}

// odd numbers

func oddArraysValuesForInToString(array: [Int]) -> String {
    var str = ""
    for item in array {
        if item % 2 == 1 {
            if str.count > 0 {
                str += ", "
            }
            str += String(item)
        }
    }
    return str
}

func oddArraysValuesWhileToString(array: [Int]) -> String {
    var str = ""
    var index = 0
    while index < array.count {
        if array[index] % 2 == 1 {
            if str.count > 0 {
                str += ", "
            }
            str += String(array[index])
        }
        index += 1
    }
    return str
}

func oddArraysValuesRepeatWhileToString(array: [Int]) -> String {
    var str = ""
    var index = 0
    repeat {
        if array[index] % 2 == 1 {
            if str.count > 0 {
                str += ", "
            }
            str += String(array[index])
        }
        index += 1
    } while index < array.count
    return str
}

// even numbers

func evenArraysValuesForInToString(array: [Int]) -> String {
    var str = ""
    for item in array {
        if item % 2 == 0 {
            if str.count > 0 {
                str += ", "
            }
            str += String(item)
        }
    }
    return str
}

func evenArraysValuesWhileToString(array: [Int]) -> String {
    var str = ""
    var index = 0
    while index < array.count {
        if array[index] % 2 == 0 {
            if str.count > 0 {
                str += ", "
            }
            str += String(array[index])
        }
        index += 1
    }
    return str
}

func evenArraysValuesRepeatWhileToString(array: [Int]) -> String {
    var str = ""
    var index = 0
    repeat {
        if array[index] % 2 == 0 {
            if str.count > 0 {
                str += ", "
            }
            str += String(array[index])
        }
        index += 1
    } while index < array.count
    return str
}


//------------------------

print("All values: " + arraysValuesForInToString(array: array))
print("All values: " + arraysValuesWhileToString(array: array))
print("All values: " + arraysValuesRepeatWhileToString(array: array))

print("Odd values: " + oddArraysValuesForInToString(array: array))
print("Odd values: " + oddArraysValuesWhileToString(array: array))
print("Odd values: " + oddArraysValuesRepeatWhileToString(array: array))

print("Even values: " + evenArraysValuesForInToString(array: array))
print("Even values: " + evenArraysValuesWhileToString(array: array))
print("Even values: " + evenArraysValuesRepeatWhileToString(array: array))
