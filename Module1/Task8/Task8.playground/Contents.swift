import Foundation


indirect enum LinkedList<T> {
    case empty
    case node(value: T, next: LinkedList<T>)
    
    var first: T? {
        switch self {
        case .empty:
            return nil
        case let .node(value, _):
            return value
        }
    }
    
    var last: T? {
        switch self {
        case .empty:
            return nil
        case let .node(value, next):
            if next.isEmpty {
                return value
            }
            return next.last
        }
    }
    
    var isEmpty: Bool {
        switch self {
        case .empty:
            return true
        default:
            return false
        }
    }
    
    var description: String {
        switch self {
        case .empty:
            return "an empty node"
        case let .node(value, next):
            return "value: \(value) " + "\(next.isEmpty ? "|| the last item of the list" : next.description )"
        }
    }
    
    init() { self = .empty }
    
    init(value: T) {
        self = .node(value: value, next: LinkedList<T>())
    }
    
    mutating func append(_ newValue: T) {
        switch self {
        case .empty:
            self = .node(value: newValue, next: LinkedList<T>())
        case .node(let value, var next):
            next.append(newValue)
            self = .node(value: value, next: next)
        }
    }
    
    func item(at index: Int) -> T? {
        if index < 0 {
            return nil
        }
        switch self {
        case .empty:
            return nil
        case let .node(value, next):
            if index == 0 {
                return value
            }
            return next.item(at: index - 1)
        }
    }
    
}

var list: LinkedList<String> = LinkedList<String>()

print(list.first ?? "nil")
print(list.last ?? "nil")


list.append("first item")
list.append("second item")
list.append("third item")
list.append("forth item")
list.append("fifth item")
list.append("sixth item")
list.append("seventh item")
list.append("nineth item")


print(list.first ?? "nil")
print(list.last ?? "nil")
print(list.item(at: 3) ?? "nil")
print(list.item(at: 21) ?? "nil")
print(list.item(at: 5) ?? "nil")
print(list.description)


