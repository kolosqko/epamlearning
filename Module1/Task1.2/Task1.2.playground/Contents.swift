import Foundation

// if there is no real solutions then func will return nil
// and if there is only 1 root it will return the same root twice

func rootsOfQuadraticEquation(_ a: Double, _ b: Double, _ c: Double) -> (Double, Double)? {
    
    if a == 0 {
        if b == 0 {
            return nil
        }
        let root = -c / b
        return (root, root)
    }
    
    let d = pow(b, 2) - 4*c*a
    if d < 0 {
        return nil
    }
    if d == 0 {
        let root = -b / (2 * a)
        return (root, root)
    }
    let root1 =  ((0 - b) + pow(d, 0.5)) / (2 * a)
    let root2 =  ((0 - b) - pow(d, 0.5)) / (2 * a)

    return (root1, root2)
}


func getQuadraticEquationString(_ a: Double, _ b: Double, _ c: Double) -> String {
    if a == 0, b == 0 {
        return "a and b are both  0, so "
    }
    return "\(a)*x^2 + \(b)*x + \(c) = 0, "
}


func getResultString(roots: (Double, Double)?) -> String {
    guard let roots = roots else {
        return "there is no real solution"
    }
    if roots.0 == roots.1 {
        return "root = \(roots.0)"
    }
    return "root1 = \(roots.0), root2 = \(roots.1)"
}


// -------------------

var a: Double = 14
var b: Double = 329
var c: Double = 233
var roots = rootsOfQuadraticEquation(a, b, c)
var string = getQuadraticEquationString(a, b, c) + getResultString(roots: roots)

print(string)

a = 1
b = 1
c = 1

roots = rootsOfQuadraticEquation(a, b, c)
string = getQuadraticEquationString(a, b, c) + getResultString(roots: roots)

print(string)


a = 0

roots = rootsOfQuadraticEquation(a, b, c)
string = getQuadraticEquationString(a, b, c) + getResultString(roots: roots)

print(string)

a = 0
b = 0

roots = rootsOfQuadraticEquation(a, b, c)
string = getQuadraticEquationString(a, b, c) + getResultString(roots: roots)

print(string)
