import UIKit


let allStudents = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
let presentOnMonday = [1, 2, 5, 6, 7]
let presentOnTuesday = [3, 6, 8, 10]
let presentOnWednesday =  [1, 3, 7, 9, 10]


enum Day: CaseIterable {
    case monday
    case tuesday
    case wednesday
    case thursday
    case friday
    case saturday
    case sunday
    
    var presentStudents: [Int] {
        switch self {
        case .monday: return presentOnMonday
        case .tuesday: return presentOnTuesday
        case .wednesday: return presentOnWednesday
        default:
            return []
        }
    }
}

struct Student {
    let studentNumber: Int
    var daysPresent: [Day] {
        return Day.allCases.filter { $0.presentStudents.contains(studentNumber)}
    }
    
    init(withNumber studentNumber: Int) {
        self.studentNumber = studentNumber
    }
}


let students = allStudents.map { Student(withNumber: $0) }


let studentsNumbersAttThreeTimes = (students.filter {$0.daysPresent.count == 3}).map { $0.studentNumber }
let studentsNumbersTwoTimes = (students.filter {$0.daysPresent.count == 2}).map { $0.studentNumber }
let studentsNumbersZeroTimes = (students.filter {$0.daysPresent.count == 0}).map { $0.studentNumber }
let studentsNumbersMonWedNotTue = (students.filter {$0.daysPresent.contains(.monday) && $0.daysPresent.contains(.wednesday) && !$0.daysPresent.contains(.tuesday) }).map { $0.studentNumber }


print("Students that attended university three days: \(studentsNumbersAttThreeTimes)")
print("Students that attended university two days: \(studentsNumbersTwoTimes)")
print("Students that attended university zero days: \(studentsNumbersZeroTimes)")
print("Students that attended university  Monday and Wednesday but not Tuesday: \(studentsNumbersMonWedNotTue)")

